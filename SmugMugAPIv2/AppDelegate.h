//
//  AppDelegate.h
//  SmugMugAPIv2
//
//  Created by Brian Gerfort of 2ndNature on 12/06/2018.
//
//  This code is in the public domain.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end

