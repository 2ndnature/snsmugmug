//
//  SNSmugMug.m
//
//  Created by Brian Gerfort of 2ndNature on 12/06/2018.
//
//  This code is in the public domain.
//

#import "SNSmugMug.h"
#import <CommonCrypto/CommonDigest.h>
#import <SafariServices/SafariServices.h>

#define API_KEY             @""
#define API_SECRET          @""
#define CALLBACK_SCHEME     @"sm-983457230946310://"

/**
 * 1. Get the API key and secret from your account settings on the SmugMug site.
 * 2. Change the CALLBACK_SCHEME numbers a to avoid collisions with other apps.
 * 3. Add the callback scheme to your info.plist. Example:
 * 4. Add "if ([SNSmugMug handleAuthorizationCallback:url]) return YES;" to the top of the application:openURL:options: method of your AppDelegate.
 *
 * <key>CFBundleURLTypes</key>
 * <array>
 *   <dict>
 *     <key>CFBundleURLSchemes</key>
 *     <array>
 *       <string>sm-983457230946310</string>
 *     </array>
 *   </dict>
 * </array>
 *
 **/

NSString * const SNSmugMugAuthorizationCallbackURLNotification = @"SNSmugMugAuthorizationCallbackURLNotification";
NSString * const kSNSmugMugAccessTokenUserDefaultKey = @"SNSmugMugAccessTokenUserDefaultKey";
NSString * const kSNSmugMugAccessTokenSecretUserDefaultKey = @"SNSmugMugAccessTokenSecretUserDefaultKey";

@interface NSString (SNSmugMug)

- (NSString *)snHMACSHA1Base64SignatureUsingKey:(NSString *)key;
- (NSDictionary<NSString *, NSString *> *)snDictionaryFromArguments;

@end

@interface NSData (SNSmugMug)

- (NSData *)snSHA1Hash;
- (NSString *)snMD5Hash;
- (NSString *)snBase64Encoded;

@end

@interface SNSmugMugSafariViewController : SFSafariViewController

@property (nonatomic, copy) void (^cancelledHandler)(SNSmugMugSafariViewController *controller);

@end

@implementation SNSmugMugSafariViewController

- (void)dismissViewControllerAnimated:(BOOL)flag completion:(void (^)(void))completion
{
    if (self.cancelledHandler != nil)
    {
        self.cancelledHandler(self);
    }
    [super dismissViewControllerAnimated:flag completion:completion];
}

@end

@interface SNSmugMugUploadCallbackBlocks : NSObject

@property (nonatomic, copy) void (^progressBlock)(int64_t bytesWritten, int64_t totalBytesWritten, int64_t totalBytesExpectedToWrite);
@property (nonatomic, copy) void (^completionHandler)(NSDictionary *result);

+ (instancetype)uploadCallbackBlocksWithProgressBlock:(void (^_Nullable)(int64_t bytesWritten, int64_t totalBytesWritten, int64_t totalBytesExpectedToWrite))progressBlock completionHandler:(void (^_Nullable)(NSDictionary *result))completionHandler;

@end

@implementation SNSmugMugUploadCallbackBlocks

+ (instancetype)uploadCallbackBlocksWithProgressBlock:(void (^_Nullable)(int64_t bytesWritten, int64_t totalBytesWritten, int64_t totalBytesExpectedToWrite))progressBlock completionHandler:(void (^_Nullable)(NSDictionary *result))completionHandler
{
    SNSmugMugUploadCallbackBlocks *uploadCallbackBlocks = [[SNSmugMugUploadCallbackBlocks alloc] init];
    uploadCallbackBlocks.progressBlock = progressBlock;
    uploadCallbackBlocks.completionHandler = completionHandler;
    return uploadCallbackBlocks;
}

@end

@interface SNSmugMug () <NSURLSessionDelegate, NSURLSessionTaskDelegate>

@property (nonatomic, copy) NSURL *baseURL;
@property (nonatomic, copy) NSString *apiKey;
@property (nonatomic, copy) NSString *secret;
@property (nonatomic, copy) NSString *oauthToken;
@property (nonatomic, copy) NSString *oauthTokenSecret;
@property (nonatomic, strong) SNSmugMugSafariViewController *authorizationViewController;
@property (nonatomic, copy) NSString *identity;
@property (nonatomic, copy) NSString *authorizedUser;
@property (nonatomic, strong) NSCharacterSet *allowedCharacterSet;
@property (nonatomic, strong) NSURLSession *uploadSession;
@property (nonatomic, strong) NSMutableDictionary<NSNumber *, SNSmugMugUploadCallbackBlocks *> *uploadCallbackBlocks;

@end

@implementation SNSmugMug

+ (NSString *)callbackScheme
{
    return CALLBACK_SCHEME;
}

+ (BOOL)handleAuthorizationCallback:(nonnull NSURL *)url
{
    if ([[url relativeString] rangeOfString:[SNSmugMug callbackScheme]].location != NSNotFound)
    {
        [[NSNotificationCenter defaultCenter] postNotificationName:SNSmugMugAuthorizationCallbackURLNotification object:url];
        return YES;
    }
    return NO;
}

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (instancetype)init
{
    return [self initWithIdentity:nil authorizationDidChangeHandler:nil];
}

- (instancetype _Nonnull)initWithIdentity:(NSString * _Nullable)identity authorizationDidChangeHandler:(void (^_Nullable)(SNSmugMug * _Nonnull smugMug))authorizationDidChangeHandler
{
    if ((self = [super init]))
    {
        NSMutableCharacterSet *characterSet = [NSMutableCharacterSet characterSetWithCharactersInString:@"`~!@#$^&*()=+[]\\{}|;':\",/<>?% "];
        [characterSet invert];
        self.allowedCharacterSet = [characterSet copy];
        self.baseURL = [NSURL URLWithString:@"https://api.smugmug.com"];
        self.identity = identity;
        self.apiKey = API_KEY;
        self.secret = API_SECRET;
        NSAssert((API_KEY.length > 0 && API_SECRET.length > 0), @"You need to fill out your API key and secret");
        self.authorizationDidChangeHandler = authorizationDidChangeHandler;
        [self loadAuthFromUserDefaults];
    }
    return self;
}

- (void)loadAuthFromUserDefaults
{
    NSString *identity = (self.identity != nil) ? self.identity : @"";
    self.oauthToken = [[NSUserDefaults standardUserDefaults] objectForKey:[NSString stringWithFormat:@"%@%@", kSNSmugMugAccessTokenUserDefaultKey, identity]];
    self.oauthTokenSecret = [[NSUserDefaults standardUserDefaults] objectForKey:[NSString stringWithFormat:@"%@%@", kSNSmugMugAccessTokenSecretUserDefaultKey, identity]];
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
        
        @autoreleasepool
        {
            [self updateAuthorizationStatus:([self authorizedUser] != nil) ? SNSmugMugAutorizationStatusAuthorized : SNSmugMugAutorizationStatusUnauthorized];
        }
    });
}

- (NSString *)authorizedUser
{
    if (_authorizedUser == nil)
    {
        __block NSString *username = nil;
        NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:[self oauthURLForPath:@"/api/v2!authuser" method:@"GET" arguments:@{}]];
        [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
        dispatch_semaphore_t semaphore = dispatch_semaphore_create(0);
        [[[NSURLSession sharedSession] dataTaskWithRequest:request completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
            NSDictionary *reply = (data != nil) ? [NSJSONSerialization JSONObjectWithData:data options:0 error:nil] : nil;
            username = [[[reply valueForKeyPath:@"Response.User.Uri"] componentsSeparatedByString:@"/"] lastObject];
            dispatch_semaphore_signal(semaphore);
            
        }] resume];
        dispatch_semaphore_wait(semaphore, dispatch_time(DISPATCH_TIME_NOW, (int64_t)((request.timeoutInterval + 2) * NSEC_PER_SEC)));
        _authorizedUser = [username copy];
    }
    return _authorizedUser;
}

- (NSURLSession *)uploadSession
{
    if (_uploadSession == nil)
    {
        _uploadSession = [NSURLSession sessionWithConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration] delegate:self delegateQueue:nil];
        _uploadCallbackBlocks = [NSMutableDictionary dictionaryWithCapacity:10];
    }
    return _uploadSession;
}

#pragma mark - Authorization

- (void)updateAuthorizationStatus:(SNSmugMugAutorizationStatus)newStatus
{
    _autorizationStatus = newStatus;
    
    if (self.authorizationDidChangeHandler != nil)
    {
        if ([NSThread isMainThread])
        {
            self.authorizationDidChangeHandler(self);
        }
        else
        {
            dispatch_async(dispatch_get_main_queue(), ^{

                self.authorizationDidChangeHandler(self);

            });
        }
    }
}

- (void)authorizeFromViewController:(nonnull UIViewController *)controller
{
    if ([controller isKindOfClass:[UIViewController class]] == NO) return;

    [self updateAuthorizationStatus:SNSmugMugAutorizationStatusAuthorizing];

    [[NSNotificationCenter defaultCenter] removeObserver:self];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(handleAuthorizationNotification:) name:SNSmugMugAuthorizationCallbackURLNotification object:nil];

    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
       
        @autoreleasepool
        {
            self.oauthToken = nil;
            self.oauthTokenSecret = nil;
            NSURL *requestTokenURL = [self oauthURLForPath:@"/services/oauth/1.0a/getRequestToken" method:@"GET" arguments:@{@"oauth_callback":[SNSmugMug callbackScheme]}];
            NSDictionary *replyParms = [[NSString stringWithContentsOfURL:requestTokenURL encoding:NSUTF8StringEncoding error:nil] snDictionaryFromArguments];
            self.oauthToken = [replyParms objectForKey:@"oauth_token"];
            self.oauthTokenSecret = [replyParms objectForKey:@"oauth_token_secret"];
            if (self.oauthToken != nil && self.oauthTokenSecret != nil)
            {
                NSURL *authURL = [self oauthURLForPath:@"/services/oauth/1.0a/authorize" method:@"GET" arguments:@{@"Access":@"Full",@"Permissions":@"Modify",@"viewportScale":@"1.0"}];
                
                dispatch_async(dispatch_get_main_queue(), ^{

                    if (@available(iOS 11.0, *))
                    {
                        SFSafariViewControllerConfiguration *config = [[SFSafariViewControllerConfiguration alloc] init];
                        [config setBarCollapsingEnabled:NO];
                        self.authorizationViewController = [[SNSmugMugSafariViewController alloc] initWithURL:authURL configuration:config];
                        [self.authorizationViewController setDismissButtonStyle:SFSafariViewControllerDismissButtonStyleCancel];
                    }
                    else
                    {
                        self.authorizationViewController = [[SNSmugMugSafariViewController alloc] initWithURL:authURL];
                    }
                    __weak __typeof__(self) weakSelf = self;
                    [self.authorizationViewController setCancelledHandler:^(SNSmugMugSafariViewController *controller) {
                        __typeof__(self) strongSelf = weakSelf; if (!strongSelf) return;
                        [strongSelf updateAuthorizationStatus:SNSmugMugAutorizationStatusUnauthorized];
                    }];
                    [controller presentViewController:self.authorizationViewController animated:YES completion:nil];
                });
            }
            else
            {
                [self updateAuthorizationStatus:SNSmugMugAutorizationStatusUnauthorized];
            }
        }
    });
}

- (void)handleAuthorizationNotification:(NSNotification *)notification
{
    if (([notification isKindOfClass:[NSNotification class]] && [notification.object isKindOfClass:[NSURL class]]) == NO) return;

    NSURL *url = notification.object;

    [self.authorizationViewController setCancelledHandler:nil];
    [self.authorizationViewController dismissViewControllerAnimated:YES completion:nil];

    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
        
        @autoreleasepool
        {
            NSString *verifier = [[url.absoluteString snDictionaryFromArguments] objectForKey:@"oauth_verifier"];
            if (verifier.length > 0)
            {
                NSURL *accessTokenURL = [self oauthURLForPath:@"/services/oauth/1.0a/getAccessToken" method:@"GET" arguments:@{@"oauth_verifier":verifier}];
                NSDictionary *replyParms = [[NSString stringWithContentsOfURL:accessTokenURL encoding:NSUTF8StringEncoding error:nil] snDictionaryFromArguments];
                if ([replyParms objectForKey:@"oauth_token"] != nil)
                {
                    NSString *identity = (self.identity != nil) ? self.identity : @"";
                    [[NSUserDefaults standardUserDefaults] setObject:[replyParms objectForKey:@"oauth_token"] forKey:[NSString stringWithFormat:@"%@%@", kSNSmugMugAccessTokenUserDefaultKey, identity]];
                    [[NSUserDefaults standardUserDefaults] setObject:[replyParms objectForKey:@"oauth_token_secret"] forKey:[NSString stringWithFormat:@"%@%@", kSNSmugMugAccessTokenSecretUserDefaultKey, identity]];
                    [[NSUserDefaults standardUserDefaults] synchronize];
                    [self loadAuthFromUserDefaults];
                }
                else
                {
                    NSLog(@"Problem: %@", replyParms);
                    [self updateAuthorizationStatus:SNSmugMugAutorizationStatusUnauthorized];
                }
            }
            else
            {
                NSLog(@"Couldn't handle URL %@", url);
                [self updateAuthorizationStatus:SNSmugMugAutorizationStatusUnauthorized];
            }
        }
    });
}

- (void)forgetAuthorization
{
    _authorizedUser = nil;
    NSString *identity = (self.identity != nil) ? self.identity : @"";
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:[NSString stringWithFormat:@"%@%@", kSNSmugMugAccessTokenUserDefaultKey, identity]];
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:[NSString stringWithFormat:@"%@%@", kSNSmugMugAccessTokenSecretUserDefaultKey, identity]];
    [[NSUserDefaults standardUserDefaults] synchronize];
    [self loadAuthFromUserDefaults];
}

#pragma mark - Requests

- (void)request:(NSString *)string method:(NSString *)method arguments:(NSDictionary *)arguments completionHandler:(void (^)(NSDictionary *result))completionHandler
{
    if (completionHandler == nil) return;
    
    if (self.autorizationStatus != SNSmugMugAutorizationStatusAuthorized)
    {
        if (completionHandler != nil)
        {
            completionHandler(@{@"Code":@"400",@"Message":@"Unauthorized"});
        }
        return;
    }

    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:[self oauthURLForPath:string method:method arguments:([arguments isKindOfClass:[NSDictionary class]]) ? arguments : @{}]];
    [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    [request setHTTPMethod:(method.length > 0) ? method : @"GET"];
    if ([method isEqualToString:@"POST"] && [arguments isKindOfClass:[NSDictionary class]])
    {
        [request setHTTPBody:[NSJSONSerialization dataWithJSONObject:arguments options:0 error:nil]];
        [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    }
    
    [[[NSURLSession sharedSession] dataTaskWithRequest:request completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
        completionHandler((data != nil) ? [NSJSONSerialization JSONObjectWithData:data options:0 error:nil] : nil);
    }] resume];
}

- (void)addNewAlbumWithTitle:(NSString *)title privacy:(SNSmugMugAlbumPrivacy)privacy completionHandler:(void (^_Nullable)(NSDictionary * _Nullable result))completionHandler
{
    if (self.autorizationStatus != SNSmugMugAutorizationStatusAuthorized)
    {
        if (completionHandler != nil)
        {
            completionHandler(@{@"Code":@"400",@"Message":@"Unauthorized"});
        }
        return;
    }
    
    if (([title isKindOfClass:[NSString class]] && title.length > 0) == NO)
    {
        if (completionHandler != nil)
        {
            completionHandler(@{@"Code":@"400",@"Message":@"Missing album title"});
        }
        return;
    }

    NSString *privacyString;
    switch (privacy)
    {
        default:
        case SNSmugMugAlbumPrivacyPrivate:
            privacyString = @"Private";
            break;
        case SNSmugMugAlbumPrivacyUnlisted:
            privacyString = @"Unlisted";
            break;
        case SNSmugMugAlbumPrivacyPublic:
            privacyString = @"Public";
            break;
    }
    
    NSMutableCharacterSet *badCharacters = [NSMutableCharacterSet alphanumericCharacterSet];
    [badCharacters invert];
    
    [self request:[NSString stringWithFormat:@"/api/v2/folder/user/%@/!albums", self.authorizedUser] method:@"POST" arguments:@{@"Title":title,@"UrlName":[[title componentsSeparatedByCharactersInSet:badCharacters] componentsJoinedByString:@"-"],@"Privacy":privacyString,@"AutoRename":@"1"} completionHandler:^(NSDictionary *result) {

        if (completionHandler != nil)
        {
            completionHandler(result);
        }
    }];
}

- (void)updloadFile:(NSURL *)fileURL toAlbum:(NSString *)albumID title:(NSString * _Nullable)title progressBlock:(nullable void (^)(int64_t bytesWritten, int64_t totalBytesWritten, int64_t totalBytesExpectedToWrite))progressBlock completionHandler:(nullable void (^)(NSDictionary *result))completionHandler
{
    if (self.autorizationStatus != SNSmugMugAutorizationStatusAuthorized)
    {
        if (completionHandler != nil)
        {
            completionHandler(@{@"message":@"Unauthorized"});
        }
        return;
    }

    if (([fileURL isKindOfClass:[NSURL class]] && [[NSFileManager defaultManager] fileExistsAtPath:fileURL.path]) == NO)
    {
        if (completionHandler != nil)
        {
            completionHandler(@{@"message":@"No file to upload"});
        }
        return;
    }
    
    if (([albumID isKindOfClass:[NSString class]] && albumID.length > 0) == NO)
    {
        if (completionHandler != nil)
        {
            completionHandler(@{@"message":@"Bad Album ID"});
        }
        return;
    }
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
        
        @autoreleasepool
        {
            NSURL *uploadURL = [NSURL URLWithString:@"https://upload.smugmug.com/"];
            NSData *data = [NSData dataWithContentsOfURL:fileURL];
            
            NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:uploadURL];
            [request setHTTPMethod:@"POST"];
            [request setHTTPBody:data];
            [request setValue:@"keep-alive" forHTTPHeaderField:@"Connection"];
            [request setValue:@"application/octet-stream" forHTTPHeaderField:@"Content-Type"];
            [request setValue:[NSString stringWithFormat:@"%lu", data.length] forHTTPHeaderField:@"Content-Length"];
            [request setValue:[data snMD5Hash] forHTTPHeaderField:@"Content-MD5"];
            [request setValue:[[NSProcessInfo processInfo] processName] forHTTPHeaderField:@"User-Agent"];
            [request setValue:@"v2" forHTTPHeaderField:@"X-Smug-Version"];
            [request setValue:@"JSON" forHTTPHeaderField:@"X-Smug-ResponseType"];
            [request setValue:fileURL.lastPathComponent forHTTPHeaderField:@"X-Smug-Filename"];
            [request setValue:[NSString stringWithFormat:@"/api/v2/album/%@", albumID] forHTTPHeaderField:@"X-Smug-AlbumUri"];
            if ([title isKindOfClass:[NSString class]] && title.length > 0)
            {
                [request setValue:title forHTTPHeaderField:@"X-Smug-Title"];
            }
            
            NSURL *oauthURL = [self oauthURLForBaseURL:uploadURL method:@"POST" arguments:@{}];
            NSMutableArray<NSString *> *oauthArgs = [NSMutableArray arrayWithCapacity:10];
            [[oauthURL.query componentsSeparatedByString:@"&"] enumerateObjectsUsingBlock:^(NSString * _Nonnull arguments, NSUInteger idx, BOOL * _Nonnull stop) {
                
                NSArray<NSString *> *components = [arguments componentsSeparatedByString:@"="];
                if (components.count == 2)
                {
                    [oauthArgs addObject:[NSString stringWithFormat:@"%@=\"%@\"", components.firstObject, components.lastObject]];
                }
            }];
            [request setValue:[NSString stringWithFormat:@"OAuth %@", [oauthArgs componentsJoinedByString:@", "]] forHTTPHeaderField:@"Authorization"];
            
            [request setHTTPBody:data];
            
            NSURLSessionTask *uploadTask = [self.uploadSession dataTaskWithRequest:request];
            [self.uploadCallbackBlocks setObject:[SNSmugMugUploadCallbackBlocks uploadCallbackBlocksWithProgressBlock:progressBlock completionHandler:completionHandler] forKey:@(uploadTask.taskIdentifier)];
            [uploadTask resume];
        }
    });
}

#pragma mark - OAuth

- (NSURL *)oauthURLForPath:(NSString *)basePath method:(NSString *)method arguments:(NSDictionary *)arguments
{
    return [self oauthURLForBaseURL:[self.baseURL URLByAppendingPathComponent:basePath] method:method arguments:arguments];
}

- (NSURL *)oauthURLForBaseURL:(NSURL *)baseURL method:(NSString *)method arguments:(NSDictionary *)arguments
{
    NSMutableDictionary *signedArguments = ([method isEqualToString:@"POST"]) ? [NSMutableDictionary dictionaryWithCapacity:10] : [NSMutableDictionary dictionaryWithDictionary:arguments];
    [signedArguments setObject:@"1.0" forKey:@"oauth_version"];
    [signedArguments setObject:[NSString stringWithFormat:@"%lu", (long)[[NSDate date] timeIntervalSince1970]] forKey:@"oauth_timestamp"];
    [signedArguments setObject:@"HMAC-SHA1" forKey:@"oauth_signature_method"];
    [signedArguments setObject:self.apiKey forKey:@"oauth_consumer_key"];
    [signedArguments setObject:[[[NSProcessInfo processInfo] globallyUniqueString] substringToIndex:8] forKey:@"oauth_nonce"];
    
    if (self.oauthToken != nil && [arguments objectForKey:@"oauth_token"] == nil)
    {
        [signedArguments setObject:self.oauthToken forKey:@"oauth_token"];
    }
    
    NSMutableArray *escapedArguments = [NSMutableArray arrayWithCapacity:signedArguments.count];
    [[[signedArguments allKeys] sortedArrayUsingSelector:@selector(compare:)] enumerateObjectsUsingBlock:^(id  _Nonnull key, NSUInteger idx, BOOL * _Nonnull stop) {
        
        [escapedArguments addObject:[NSString stringWithFormat:@"%@=%@", key, [[[signedArguments objectForKey:key] description] stringByAddingPercentEncodingWithAllowedCharacters:self.allowedCharacterSet]]];
        
    }];
    
    NSMutableString *message = [NSMutableString string];
    [message appendString:method];
    [message appendString:@"&"];
    [message appendString:[[baseURL absoluteString] stringByAddingPercentEncodingWithAllowedCharacters:self.allowedCharacterSet]];
    [message appendString:@"&"];
    [message appendString:[[escapedArguments componentsJoinedByString:@"&"] stringByAddingPercentEncodingWithAllowedCharacters:self.allowedCharacterSet]];
    [signedArguments setObject:[message snHMACSHA1Base64SignatureUsingKey:(self.oauthTokenSecret != nil) ? [NSString stringWithFormat:@"%@&%@", self.secret, self.oauthTokenSecret] : [NSString stringWithFormat:@"%@&", self.secret]] forKey:@"oauth_signature"];
    
    NSMutableArray *query = [NSMutableArray arrayWithCapacity:signedArguments.count];
    [signedArguments enumerateKeysAndObjectsUsingBlock:^(id  _Nonnull key, id  _Nonnull obj, BOOL * _Nonnull stop) {
        [query addObject:[NSString stringWithFormat:@"%@=%@", key, [[obj description] stringByAddingPercentEncodingWithAllowedCharacters:self.allowedCharacterSet]]];
    }];
    
    return [NSURL URLWithString:[NSString stringWithFormat:@"%@?%@", [baseURL absoluteString], [query componentsJoinedByString:@"&"]]];
}

#pragma mark - NSURLSessionDelegate

- (void)URLSession:(NSURLSession *)session task:(NSURLSessionTask *)task didSendBodyData:(int64_t)bytesSent totalBytesSent:(int64_t)totalBytesSent totalBytesExpectedToSend:(int64_t)totalBytesExpectedToSend
{
    SNSmugMugUploadCallbackBlocks *uploadCallbackBlocks = [self.uploadCallbackBlocks objectForKey:@(task.taskIdentifier)];
    if (uploadCallbackBlocks.progressBlock != nil)
    {
        uploadCallbackBlocks.progressBlock(bytesSent, totalBytesSent, totalBytesExpectedToSend);
    }
}

- (void)URLSession:(NSURLSession *)session dataTask:(NSURLSessionDataTask *)dataTask didReceiveData:(NSData *)data
{
    SNSmugMugUploadCallbackBlocks *uploadCallbackBlocks = [self.uploadCallbackBlocks objectForKey:@(dataTask.taskIdentifier)];
    if (uploadCallbackBlocks.completionHandler != nil)
    {
        NSDictionary *result = (data != nil) ? [NSJSONSerialization JSONObjectWithData:data options:0 error:nil] : nil;
        if (result != nil)
        {
            uploadCallbackBlocks.completionHandler(result);
        }
        else
        {
            uploadCallbackBlocks.completionHandler((data != nil) ? @{@"message":[[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding]} : nil);
        }
    }
}

- (void)URLSession:(NSURLSession *)session task:(NSURLSessionTask *)task didCompleteWithError:(nullable NSError *)error
{
    if (error != nil)
    {
        SNSmugMugUploadCallbackBlocks *uploadCallbackBlocks = [self.uploadCallbackBlocks objectForKey:@(task.taskIdentifier)];
        uploadCallbackBlocks.completionHandler(@{@"message":[error localizedDescription]});
    }
    [self.uploadCallbackBlocks removeObjectForKey:@(task.taskIdentifier)];
}

@end

#pragma mark - Helper Categories

static char encodingTable[64] = {
    'A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P',
    'Q','R','S','T','U','V','W','X','Y','Z','a','b','c','d','e','f',
    'g','h','i','j','k','l','m','n','o','p','q','r','s','t','u','v',
    'w','x','y','z','0','1','2','3','4','5','6','7','8','9','+','/' };

@implementation NSString (SNSmugMug)

- (NSString *)snHMACSHA1Base64SignatureUsingKey:(NSString *)key
{
    NSData *keyData = [key dataUsingEncoding:NSUTF8StringEncoding];
    
    if (keyData.length > CC_SHA1_BLOCK_BYTES)
    {
        keyData = [keyData snSHA1Hash];
    }
    
    if (keyData.length < CC_SHA1_BLOCK_BYTES)
    {
        NSUInteger padSize = CC_SHA1_BLOCK_BYTES - keyData.length;
        NSMutableData *paddedData = [NSMutableData dataWithData:keyData];
        [paddedData appendData:[NSMutableData dataWithLength:padSize]];
        keyData = paddedData;
    }
    
    NSMutableData *oKeyPad = [NSMutableData dataWithLength:CC_SHA1_BLOCK_BYTES];
    NSMutableData *iKeyPad = [NSMutableData dataWithLength:CC_SHA1_BLOCK_BYTES];
    
    const uint8_t *kdPtr = [keyData bytes];
    uint8_t *okpPtr = [oKeyPad mutableBytes];
    uint8_t *ikpPtr = [iKeyPad mutableBytes];
    
    memset(okpPtr, 0x5c, CC_SHA1_BLOCK_BYTES);
    memset(ikpPtr, 0x36, CC_SHA1_BLOCK_BYTES);
    
    NSUInteger i;
    for (i = 0; i < CC_SHA1_BLOCK_BYTES; i++)
    {
        okpPtr[i] = okpPtr[i] ^ kdPtr[i];
        ikpPtr[i] = ikpPtr[i] ^ kdPtr[i];
    }
    
    NSData *msgData = [self dataUsingEncoding:NSUTF8StringEncoding];
    
    NSMutableData *innerData = [NSMutableData dataWithData:iKeyPad];
    [innerData appendData:msgData];
    NSData *innerDataHashed = [innerData snSHA1Hash];
    
    NSMutableData *outerData = [NSMutableData dataWithData:oKeyPad];
    [outerData appendData:innerDataHashed];
    
    return [[outerData snSHA1Hash] snBase64Encoded];
}

- (NSDictionary<NSString *, NSString *> *)snDictionaryFromArguments
{
    NSArray *vars = [self componentsSeparatedByString:@"&"];
    NSMutableDictionary *result = [NSMutableDictionary dictionaryWithCapacity:vars.count];
    NSArray *comps;
    for (NSString *var in vars)
    {
        comps = [var componentsSeparatedByString:@"="];
        if (comps.count == 2)
        {
            [result setObject:comps.lastObject forKey:comps.firstObject];
        }
    }
    return result;
}

@end

@implementation NSData (SNSmugMug)

- (NSData *)snSHA1Hash
{
    NSMutableData *result = [NSMutableData dataWithLength:CC_SHA1_DIGEST_LENGTH];
    CC_SHA1_CTX context;
    CC_SHA1_Init(&context);
    CC_SHA1_Update(&context, [self bytes], (CC_LONG)self.length);
    CC_SHA1_Final([result mutableBytes], &context);
    return result;
}

- (NSString *)snMD5Hash
{
    unsigned char digest[16];
    char result[32];
    int i;
    
    CC_MD5([self bytes], (CC_LONG)self.length, digest);
    for (i = 0; i < 16; i++) sprintf(result + i * 2, "%02x", digest[i]);
    
    return [NSString stringWithCString:result encoding:NSUTF8StringEncoding];
}

- (NSString *)snBase64Encoded
{
    unsigned char inBuffer[3], outBuffer[4];
    unsigned short i = 0, charactersOnLine = 0, charactersToCopy = 0;
    long remainingCharacters = 0;
    unsigned long stringLength = self.length, stringPosition = 0, linePosition = 0;
    NSMutableString *result = [NSMutableString stringWithCapacity:stringLength];

    const unsigned char *bytes = [self bytes];
    
    while (1)
    {
        remainingCharacters = stringLength - stringPosition;
        if (remainingCharacters <= 0) break;
        
        for (i = 0; i < 3; i++)
        {
            linePosition = stringPosition + i;
            if (linePosition < stringLength) inBuffer[i] = bytes[linePosition];
            else inBuffer[i] = 0;
        }
        
        outBuffer[0] = (inBuffer[0] & 0xFC) >> 2;
        outBuffer[1] = ((inBuffer[0] & 0x03) << 4) | ((inBuffer[1] & 0xF0) >> 4);
        outBuffer[2] = ((inBuffer[1] & 0x0F) << 2) | ((inBuffer[2] & 0xC0) >> 6);
        outBuffer[3] = inBuffer[2] & 0x3F;
        charactersToCopy = 4;
        
        switch (remainingCharacters)
        {
            case 1:
                charactersToCopy = 2;
                break;
            case 2:
                charactersToCopy = 3;
                break;
        }
        
        for (i = 0; i < charactersToCopy; i++)
        {
            [result appendFormat:@"%c", encodingTable[outBuffer[i]]];
        }
        
        for (i = charactersToCopy; i < 4; i++)
        {
            [result appendString:@"="];
        }
        
        stringPosition += 3;
        charactersOnLine += 4;
    }
    
    return [NSString stringWithString:result];
}

@end
