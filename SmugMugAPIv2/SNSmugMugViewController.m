//
//  SNSmugMugViewController.m
//  SmugMugAPIv2
//
//  Created by Brian Gerfort of 2ndNature on 12/06/2018.
//
//  This code is in the public domain.
//

#import "SNSmugMugViewController.h"

@interface SNSmugMugViewController ()

@property (nonatomic, readonly) SNSmugMug *smugMug;
@property (nonatomic, strong) NSMutableArray<NSDictionary *> *rows;
@property (nonatomic, strong) UIProgressView *progressView;

@end

@implementation SNSmugMugViewController

static NSString *CellIdentifier = @"CellIdentifier";

#pragma mark - User Interface

- (void)viewDidLoad
{
    [super viewDidLoad];

    self.title = NSLocalizedString(@"SmugMug", NULL);

    self.rows = [NSMutableArray arrayWithCapacity:10];

    [self.tableView registerClass:[UITableViewCell class] forCellReuseIdentifier:CellIdentifier];
    [self.tableView setEstimatedRowHeight:44.0];
    [self.tableView setRowHeight:UITableViewAutomaticDimension];
    
    self.refreshControl = [[UIRefreshControl alloc] init];
    [self.refreshControl addTarget:self action:@selector(reloadAlbums:) forControlEvents:UIControlEventValueChanged];

    __weak __typeof__(self) weakSelf = self;
    _smugMug = [[SNSmugMug alloc] initWithIdentity:nil authorizationDidChangeHandler:^(SNSmugMug * _Nonnull smugMug) {
        
        __typeof__(self) strongSelf = weakSelf; if (!strongSelf) return;
        [strongSelf updateUI];
        [strongSelf reloadAlbums:nil completionHandler:^(BOOL success) {
            
            [UIView animateWithDuration:0.3 animations:^{
                
                [strongSelf refreshAppearance];
                
            }];
        }];
    }];
    
    [self updateUI];
}

- (void)updateUI
{
    switch (self.smugMug.autorizationStatus)
    {
        case SNSmugMugAutorizationStatusUnauthorized:
        {
            [self.rows removeAllObjects];
            [self.tableView reloadData];
            self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"Authorize", NULL) style:UIBarButtonItemStylePlain target:self action:@selector(authorize:)];
            self.navigationItem.rightBarButtonItem = nil;
            break;
        }
        case SNSmugMugAutorizationStatusAuthorizing:
        {
            UIActivityIndicatorView *spinner = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
            [spinner startAnimating];
            self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:spinner];
            self.navigationItem.rightBarButtonItem = nil;
            break;
        }
        case SNSmugMugAutorizationStatusAuthorized:
        {
            self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"Reset", NULL) style:UIBarButtonItemStylePlain target:self action:@selector(reset:)];
            self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemAdd target:self action:@selector(newAlbum:)];
            break;
        }
    }
    [self.navigationController.view layoutIfNeeded];
}

- (void)refreshAppearance
{
    BOOL authorized = (self.smugMug.autorizationStatus == SNSmugMugAutorizationStatusAuthorized);
    
    [[UIView appearance] setTintColor:(authorized) ? [UIColor colorWithRed:130.0/255.0 green:181.0/255.0 blue:54.0/255.0 alpha:1.0] : nil];
    [[UINavigationBar appearance] setBarStyle:(authorized) ? UIBarStyleBlack : UIBarStyleDefault];
    [[UITableView appearance] setBackgroundColor:(authorized) ? [UIColor colorWithWhite:0.08 alpha:1.0] : [UIColor colorWithRed:0.937255 green:0.937255 blue:0.956863 alpha:1.0]];
    [[UITableView appearance] setIndicatorStyle:(authorized) ? UIScrollViewIndicatorStyleWhite : UIScrollViewIndicatorStyleDefault];
    [[UITableView appearance] setSeparatorColor:(authorized) ? [UIColor colorWithWhite:0.2 alpha:1.0] : nil];
    [[UITableViewCell appearance] setBackgroundColor:(authorized) ? [UIColor colorWithWhite:0.11 alpha:1.0] : [UIColor whiteColor]];
    [[UILabel appearanceWhenContainedInInstancesOfClasses:@[[UITableViewHeaderFooterView class]]] setTextColor:(authorized) ? [UIColor lightGrayColor] : [UIColor colorWithRed:0.43 green:0.43 blue:0.45 alpha:1.0]];
    
    for (UIWindow *window in [[UIApplication sharedApplication] windows])
    {
        for (UIView *view in window.subviews)
        {
            [view removeFromSuperview];
            [window addSubview:view];
        }
    }
}

- (void)reloadAlbums:(id)sender
{
    [self reloadAlbums:sender completionHandler:nil];
}

- (void)reloadAlbums:(id)sender completionHandler:(void (^)(BOOL success))completionHandler
{
    [self.rows removeAllObjects];
    [self.tableView reloadData];
    
    if (self.smugMug.autorizationStatus != SNSmugMugAutorizationStatusAuthorized)
    {
        if (sender != nil)
        {
            [self.refreshControl endRefreshing];
        }
        if (completionHandler != nil)
        {
            completionHandler(NO);
        }
        return;
    }
 
    if (sender == nil)
    {
        [self.refreshControl beginRefreshing];
    }
    
    [self.smugMug request:[NSString stringWithFormat:@"/api/v2/user/%@!albums", self.smugMug.authorizedUser] method:@"GET" arguments:nil completionHandler:^(NSDictionary *result) {

        dispatch_async(dispatch_get_main_queue(), ^{
            
            [self.rows setArray:[result valueForKeyPath:@"Response.Album"]];
            [self.tableView reloadData];
            [self.refreshControl endRefreshing];
            if (completionHandler != nil)
            {
                completionHandler(YES);
            }
        });
    }];
}

#pragma mark - Authorization

- (void)authorize:(id)sender
{
    [self.smugMug authorizeFromViewController:self];
}

- (void)reset:(id)sender
{
    [self.smugMug forgetAuthorization];
}

#pragma mark - Sample Functionality

- (void)newAlbum:(id)sender
{
    if (self.smugMug.authorizedUser == nil) return;
        
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"Title for new album:", NULL) message:nil preferredStyle:UIAlertControllerStyleAlert];
    
    [alert addTextFieldWithConfigurationHandler:^(UITextField *textField) {
        
        [textField setClearButtonMode:UITextFieldViewModeWhileEditing];
        
    }];
    
    [alert addAction:[UIAlertAction actionWithTitle:NSLocalizedString(@"Create", NULL) style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        
        NSString *albumTitle = [(UITextField *)[[alert textFields] firstObject] text];
        if (albumTitle.length > 0)
        {
            __weak __typeof__(self) weakSelf = self;
            [self.smugMug addNewAlbumWithTitle:albumTitle privacy:SNSmugMugAlbumPrivacyPrivate completionHandler:^(NSDictionary * _Nullable result) {

                dispatch_async(dispatch_get_main_queue(), ^{

                    __typeof__(self) strongSelf = weakSelf; if (!strongSelf) return;
                    
                    if ((int)([[result valueForKey:@"Code"] integerValue] / 200) == 1)
                    {
                        [strongSelf reloadAlbums:nil];
                    }
                    else
                    {
                        UIAlertController *alert = [UIAlertController alertControllerWithTitle:nil message:NSLocalizedString(@"Could not create the album", nil) preferredStyle:UIAlertControllerStyleAlert];
                        [alert addAction:[UIAlertAction actionWithTitle:NSLocalizedString(@"OK", NULL) style:UIAlertActionStyleCancel handler:nil]];
                        [strongSelf presentViewController:alert animated:YES completion:nil];
                        NSLog(@"Failed to create SmugMug album '%@': %@", albumTitle, result);
                    }
                });
            }];
        }
    }]];
    
    [alert addAction:[UIAlertAction actionWithTitle:NSLocalizedString(@"Cancel", NULL) style:UIAlertActionStyleCancel handler:NULL]];
    
    [self presentViewController:alert animated:YES completion:nil];
}

- (void)deleteAlbumAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [self.tableView cellForRowAtIndexPath:indexPath];
    
    if (cell == nil) return;

    UIAlertController *confirmationAlert = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"Are you sure you want to delete this album?", NULL) message:nil preferredStyle:UIAlertControllerStyleActionSheet];
    
    UIPopoverPresentationController *pc = [confirmationAlert popoverPresentationController];
    [pc setPermittedArrowDirections:UIPopoverArrowDirectionAny];
    [pc setSourceView:cell];
    [pc setSourceRect:cell.bounds];
    
    [confirmationAlert addAction:[UIAlertAction actionWithTitle:[NSString stringWithFormat:NSLocalizedString(@"Delete '%@'", NULL), [self.rows[indexPath.row] objectForKey:@"Title"]] style:UIAlertActionStyleDestructive handler:^(UIAlertAction *action) {
        
        [self.smugMug request:[NSString stringWithFormat:@"/api/v2/album/%@", [self.rows[indexPath.row] objectForKey:@"AlbumKey"]] method:@"DELETE" arguments:nil completionHandler:^(NSDictionary *result) {
            
            if ((int)([[result valueForKey:@"Code"] integerValue] / 200) == 1)
            {
                [self.rows removeObjectAtIndex:indexPath.row];
                dispatch_async(dispatch_get_main_queue(), ^{
                    
                    [self.tableView beginUpdates];
                    [self.tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
                    [self.tableView endUpdates];
                });
            }
            else
            {
                NSLog(@"Failed: %@", result);
            }
        }];
    }]];
    
    [confirmationAlert addAction:[UIAlertAction actionWithTitle:NSLocalizedString(@"Cancel", NULL) style:UIAlertActionStyleCancel handler:nil]];
    
    [self presentViewController:confirmationAlert animated:YES completion:nil];
}

- (void)uploadPhotoToAlbumAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *albumID = [self.rows[indexPath.row] objectForKey:@"AlbumKey"];
    if (albumID == nil) return;

    if (self.progressView == nil)
    {
        self.progressView = [[UIProgressView alloc] initWithProgressViewStyle:UIProgressViewStyleBar];
        [_progressView setTranslatesAutoresizingMaskIntoConstraints:NO];
        [self.navigationController.navigationBar addSubview:_progressView];
        [[_progressView.heightAnchor constraintEqualToConstant:1.0] setActive:YES];
        [[_progressView.bottomAnchor constraintEqualToAnchor:_progressView.superview.bottomAnchor] setActive:YES];
        [[_progressView.leftAnchor constraintEqualToAnchor:_progressView.superview.leftAnchor] setActive:YES];
        [[_progressView.rightAnchor constraintEqualToAnchor:_progressView.superview.rightAnchor] setActive:YES];
    }

    __weak __typeof__(self) weakSelf = self;
    [self.smugMug updloadFile:[[NSBundle mainBundle] URLForResource:@"DSCF3072" withExtension:@"JPG"] toAlbum:albumID title:nil progressBlock:^(int64_t bytesWritten, int64_t totalBytesWritten, int64_t totalBytesExpectedToWrite) {
        
        dispatch_async(dispatch_get_main_queue(), ^{
            __typeof__(self) strongSelf = weakSelf; if (!strongSelf) return;
            [strongSelf.progressView setProgress:totalBytesWritten/(double)totalBytesExpectedToWrite];
        });
        
    } completionHandler:^(NSDictionary *result) {
        
        dispatch_async(dispatch_get_main_queue(), ^{
            __typeof__(self) strongSelf = weakSelf; if (!strongSelf) return;
            [strongSelf.progressView removeFromSuperview];
            strongSelf.progressView = nil;
        });
    }];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.rows.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];

    cell.textLabel.text = [self.rows[indexPath.row] objectForKey:@"Title"];
    
    return cell;
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    return (self.rows.count > 0) ? NSLocalizedString(@"Tap an album to upload a sample photo", NULL) : [super tableView:tableView titleForHeaderInSection:section];
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return (self.rows.count > 0) ? 20.0 : [super tableView:tableView heightForHeaderInSection:section];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    if (self.progressView == nil)
    {
        [self uploadPhotoToAlbumAtIndexPath:indexPath];
    }
}

- (NSArray<UITableViewRowAction *> *)tableView:(UITableView *)tableView editActionsForRowAtIndexPath:(NSIndexPath *)indexPath
{
    __weak __typeof__(self) weakSelf = self;
    return @[[UITableViewRowAction rowActionWithStyle:UITableViewRowActionStyleDestructive title:NSLocalizedString(@"Delete", NULL) handler:^(UITableViewRowAction *action, NSIndexPath *indexPath) {
        
        __typeof__(self) strongSelf = weakSelf; if (!strongSelf) return;
        [strongSelf deleteAlbumAtIndexPath:indexPath];
        
    }]];
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    [cell.textLabel setTextColor:(self.smugMug.autorizationStatus == SNSmugMugAutorizationStatusAuthorized) ? [UIColor whiteColor] : [UIColor blackColor]];
}

@end
