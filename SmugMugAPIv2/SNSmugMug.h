//
//  SNSmugMug.h
//
//  Created by Brian Gerfort of 2ndNature on 12/06/2018.
//
//  This code is in the public domain.
//

#import <UIKit/UIKit.h>

typedef enum : NSUInteger {
    SNSmugMugAutorizationStatusUnauthorized,
    SNSmugMugAutorizationStatusAuthorizing,
    SNSmugMugAutorizationStatusAuthorized,
} SNSmugMugAutorizationStatus;

typedef enum : NSUInteger {
    SNSmugMugAlbumPrivacyPrivate,
    SNSmugMugAlbumPrivacyUnlisted,
    SNSmugMugAlbumPrivacyPublic,
} SNSmugMugAlbumPrivacy;

@interface SNSmugMug : NSObject

@property (nonatomic, readonly) SNSmugMugAutorizationStatus autorizationStatus;
@property (nonatomic, readonly) NSString * _Nullable identity;
@property (nonatomic, readonly) NSString * _Nullable authorizedUser;
@property (nonatomic, copy) void (^ _Nullable authorizationDidChangeHandler)(SNSmugMug * _Nonnull smugMug);

- (instancetype _Nonnull)initWithIdentity:(NSString * _Nullable)identity authorizationDidChangeHandler:(void (^_Nullable)(SNSmugMug * _Nonnull smugMug))authorizationDidChangeHandler;

+ (NSString * _Nonnull)callbackScheme;
+ (BOOL)handleAuthorizationCallback:(NSURL * _Nonnull)url;
- (void)authorizeFromViewController:(UIViewController * _Nonnull)controller;
- (void)forgetAuthorization;

- (void)request:(nonnull NSString *)string method:(NSString * _Nonnull)method arguments:(NSDictionary * _Nullable)arguments completionHandler:(void (^_Nullable)(NSDictionary * _Nullable result))completionHandler;

- (void)addNewAlbumWithTitle:(NSString * _Nonnull)title privacy:(SNSmugMugAlbumPrivacy)privacy completionHandler:(void (^_Nullable)(NSDictionary * _Nullable result))completionHandler;
- (void)updloadFile:(NSURL * _Nonnull)fileURL toAlbum:(NSString * _Nonnull)albumID title:(NSString * _Nullable)title progressBlock:(void (^_Nullable)(int64_t bytesWritten, int64_t totalBytesWritten, int64_t totalBytesExpectedToWrite))progressBlock completionHandler:(void (^_Nullable)(NSDictionary * _Nullable result))completionHandler;

@end
