//
//  AppDelegate.m
//  SmugMugAPIv2
//
//  Created by Brian Gerfort of 2ndNature on 12/06/2018.
//
//  This code is in the public domain.
//

#import "AppDelegate.h"
#import "SNSmugMugViewController.h"

@implementation AppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    [_window setRootViewController:[[UINavigationController alloc] initWithRootViewController:[[SNSmugMugViewController alloc] initWithStyle:UITableViewStyleGrouped]]];
    [_window makeKeyAndVisible];
    return YES;
}

- (BOOL)application:(UIApplication *)app openURL:(NSURL *)url options:(NSDictionary<NSString *, id> *)options
{
    if ([SNSmugMug handleAuthorizationCallback:url]) return YES;
    
    // Handle other open-URL schemes
    
    return NO;
}

@end
