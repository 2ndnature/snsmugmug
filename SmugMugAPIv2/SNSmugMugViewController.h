//
//  SNSmugMugViewController.h
//  SmugMugAPIv2
//
//  Created by Brian Gerfort of 2ndNature on 12/06/2018.
//
//  This code is in the public domain.
//

#import <UIKit/UIKit.h>
#import "SNSmugMug.h"

@interface SNSmugMugViewController : UITableViewController

@end
