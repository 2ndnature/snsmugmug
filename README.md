# SNSmugMug #

### Objective-C sample code for authenticating and communicating with v2.0 of the SmugMug API ###

Add the API key and secret from your SmugMug account page to the defines at the top of SNSmugMug.m file and you should be good to go.

Public Domain code, so do as you please with it. :)
